# SPDX-FileCopyrightText: 2021 Carl Schwan <carlschwan@kde.org>
# SPDX-FileCopyrightText: 2021 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: CC0-1.0
from setuptools import setup

setup()
