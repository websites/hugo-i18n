<!--
SPDX-FileCopyrightText: 2021 Carl Schwan <carlschwan@kde.org>
SPDX-FileCopyrightText: 2021-2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# I18n tool for Hugo projects

A Python package to support the translation of [Hugo](https://gohugo.io/) projects with
[gettext](https://www.gnu.org/software/gettext/).

## Description

Following terminologies and workflows of [KDE's Scripty](https://invent.kde.org/sysadmin/l10n-scripty), `hugo-i18n`
defines two procedures for working with PO files in the process of translating a Hugo website: working with one PO file
and working with a directory of PO file(s).

In each procedure, there are three main steps: _extraction_ (to produce POT files from source files in English),
_compilation_ (to make binary files from PO files), and _generation_ (to generate files in target languages using binary
files from _compilation_ step). A _fetch_ step (to download PO files from [KDE SVN](https://websvn.kde.org/) into the
required structure) is needed for projects that handle the _generation_ step themselves instead of using Scripty.

The `FILENAME` environment variable is used to determine which procedure is in operation, and together with the
`PACKAGE` environment variable, to name fields and files. `$PACKAGE` contains the KDE project ID of the Hugo project
that `hugo-i18n` is working on. For example, for the project of the [kate-editor.org](https://kate-editor.org/) website,
`$PACKAGE` is "websites-kate-editor-org".

In the table below:
- `"<domain>" content messages` stands for messages from content files listed in the `[i18n.content.<domain>]` config
block (see the [configuration](#all-effective-configuration) section below);
- `other messages` stands for strings from `i18n/en.yaml` file and `data` folder, and site title, site description, and
menu entry names from `config.yaml`;
- `<default domain>` stands for the value of `$PACKAGE` without the namespace prefix ("websites-" or "documentation-").  
  Exceptions due to historical reason:
  - [timeline.kde.org](https://timeline.kde.org): `$PACKAGE` is "websites-timeline-kde-org", `<default domain>` is
  "timeline-kde-org-shared".

| Procedure<br>\ <br>Step | PO_FILE<br>`$FILENAME` is non-empty | PO_DIR<br>`$FILENAME` is not non-empty |
|---|---|---|
| Extraction | - Input: Path of a POT file<br>- Extracts `"default" content messages` and `other messages` into that file. | - Input: Path of a directory containing POT file(s) to be produced, the number of them are determined by configs in `config.yaml`<br>- Extracts `other messages` and `"default" content messages` (if the `[i18n.content.default]` config block exists) into `<default domain>.pot`<br>- Extracts `"<X>" content messages` ("\<X\>" is not "default") into `<X>.pot` |
| Fetch | - Input: Path of a directory<br>- Downloads the PO file with `FILENAME` as the base name in each language into the directory, file names are changed to language codes `<lang>.po` | - Input: Path of a directory<br>- Downloads PO files into subfolders of the directory, one folder for each language, file names are not changed `<lang>/<X>.po` |
| Compilation | - Input: Same as input of `fetch` step<br>- Compiles each PO file `<lang>.po` to a MO file `$FILENAME.mo` in `locale/<lang>/LC_MESSAGES` | - Input: Same as input of `fetch` step<br>- Compiles each PO file `<lang>/<X>.po` to a MO file `<X>.mo` in `locale/<lang>/LC_MESSAGES` |
| Generation | Generates translations for `other messages` and `"default" content messages` using `$FILENAME.mo` in `locale/<lang>/LC_MESSAGES` | Generates translations for `other messages` and all `"<X>" content messages` using `<X>.mo` files in `locale/<lang>/LC_MESSAGES` |

## All effective configuration

`hugo-i18n` uses Hugo's `config.yaml` to store configs for a project:

```yaml
i18n: # required to use hugo-i18n
  content: # hugo-i18n will work on content files
    default: # required for PO_FILE procedure, optional for PO_DIR procedure
      files: # single files
        - content/a_name_only_for_demonstration.md
        - content/another_one.md
      globs: # multiple files with similar paths/names
        - content/some_folder/*.md
        - content/files_numbered_from_1_to_100/2*.md
      excludedFiles: # single files to exclude from those listed above
        - content/some_folder/to_be_excluded.md
      excludedGlobs: # multiple files to exclude from those listed above
        - content/files_numbered_from_1_to_100/2[2-5].md
    # there must be at least `globs` or `files` key in a domain listing dict
    # only `default` is a fixed key name, names for other keys of the `content` dict are by choice
    domain_only_globs:
      globs:
        - content/announcements/13/13.1[1-3].0/*
        - content/announcements/13/13.11.*.md
        - content/announcements/cog/*.md
    domain_only_files:
      files:
        - content/_index.md
  data: # hugo-i18n will work on data files
    # similar to a domain listing dict inside `content` dict
    files: # single files
      - data/demonstrative_name.yaml # currently supports only YAML files
      - data/directory/file_in_a_folder.yaml
    globs: # multiple files with similar paths/names
      - data/another_folder/*_info.yaml
      - data/*.yaml
  # hugo-i18n will work on strings in `i18n/` folder and site title, site description, and menu entry names in `config.yaml`
  others: [strings, menu, title, description]
  # whether to leave `srcDir` directory only for English files
  # by default, this is false, hugo-i18n will generate target files in the same location as English ones, only change the language code in file names
  # if true, hugo-i18n will generate target files in `genDir` directory, replacing `{srcDir}/` part in each file path with `{genDir}/{language_code}/`
  genToOtherDir: true
  srcDir: content # directory for English files, default to 'content'
  genDir: content-trans # each subdirectory of this directory is for one language. Default to 'content-trans'
  # a space-separated string of YAML keys to exclude from extraction and generation of frontmatters
  # this custom key list will be joined with the default key list in `config.py` module to make the full list of excluded frontmatter keys
  excludedKeys: ''
  # similarly, a space-separated string of YAML keys to exclude from extraction and generation of data files
  # there is no default key list for this
  excludedDataKeys: ''
  # specify which params of each shortcode to be i12ized
  shortcodes:
    # {k, v} with k: shortcode name, v: a list with each item being either the name or 0-based index of a param (v can contain both)
    params:
      i18n_var: [1]
      figure: [alt, caption]
      '*': [title] # process `title` param of all shortcodes
```

Each content file can also specify conditions in its frontmatter to consider whether its translation in a language is
good enough.
Each condition is the required translation percentage of another content file or of the string file.
A translation of the content file is considered good enough when all conditions are met for that translation.

```yaml
i18n_configs: # required to specify conditions
  conditions: # required to specify conditions
    # each condition can be either a string or a dict of (string, float). In case of a string, the float is default to be 0.75
    # the string is either "strings" for the string file, or the path to a content file.
    # the float is the threshold for qualifying the file's translation.
    - strings: 0.8 # "strings" is a fixed name
    - content/another-file.md
```

If at least one condition is not met, a `warning: true` pair will be added to the `i18n_configs` dict, which then can be
used by the page layout to e.g. show a warning.

## Usage

- The main command is `hugoi18n`
- 4 subcommands are verbs corresponding to name of the 4 steps, the input of each subcommand is also described in the
table above
- Remember to set environment variables before running commands.

## License

This project is licensed under LGPL-2.1-or-later.

## Development

- With Conda

```bash
conda env create -f environment.yml
conda activate hi
```