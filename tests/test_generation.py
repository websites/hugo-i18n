# SPDX-FileCopyrightText: 2021-2022 Phu Hung Nguyen <phu@kde.org>
# SPDX-License-Identifier: LGPL-2.1-or-later

from __future__ import annotations

import importlib.resources as pkg_resources
import os
import shutil
import unittest
from argparse import Namespace
from typing import Tuple

from hugo_gettext.generation.g_domain import HugoDomainG
from hugo_gettext.generation.renderer_hugo_l10n import RendererHugoL10N
from markdown_gettext.domain_generation import gettext_func
from markdown_it import MarkdownIt
from mdit_py_hugo.shortcode import shortcode_plugin
from mdit_py_i18n.utils import L10NResult
from mdit_py_plugins.front_matter import front_matter_plugin
from mdit_py_plugins.deflist import deflist_plugin


class GenerationTestCase(unittest.TestCase):
    package = 'websites-kde-org'
    os.environ['PACKAGE'] = package
    mdi = MarkdownIt(renderer_cls=RendererHugoL10N).use(front_matter_plugin).use(shortcode_plugin) \
        .enable('table').use(deflist_plugin)
    locale_path = 'locale/vi/LC_MESSAGES'
    os.makedirs(locale_path, exist_ok=True)
    os.environ["LANGUAGE"] = 'vi'
    lang_g = Namespace(
        g=Namespace(
            hg_config=Namespace(
                excluded_keys=[],
                shortcodes={}
            ),
            src_strings={}
        ),
        l10n_results={}
    )

    def test_generate_content(self):
        fm_result, content_result = prepare(self, 'distros')
        self.assertEqual(1, content_result.rate)

    def test_generate_blockquotes(self):
        fm_result, content_result = prepare(self, 'blockquotes')
        self.assertEqual(1, content_result.rate)

    def test_generate_codeblock(self):
        fm_result, content_result = prepare(self, 'codeblock')
        print(content_result.localized)
        self.assertEqual(1, content_result.rate)

    def test_generate_deflist(self):
        fm_result, content_result = prepare(self, 'deflist')
        print(content_result.localized)
        self.assertEqual(10, content_result.localized.count(': '))
        self.assertEqual(65, len(content_result.localized.split('\n')))


def prepare(test: GenerationTestCase, domain: str) -> Tuple[L10NResult, L10NResult]:
    with pkg_resources.path('tests.resources', f'{domain}.mo') as p:
        shutil.copyfile(p, f'{test.locale_path}/{domain}.mo')
    domain_g = HugoDomainG(test.lang_g, gettext_func(domain))
    env = {
        'parse_fence': True,
        'domain_generation': domain_g,
    }
    with pkg_resources.open_text('tests.resources', f'{domain}.md') as f:
        return test.mdi.render(f.read(), env)


if __name__ == '__main__':
    unittest.main()
