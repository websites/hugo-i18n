# SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
# SPDX-License-Identifier: LGPL-2.1-or-later

import os
import shutil
import unittest

from hugoi18n.fetch import fetch_langs


class FetchTestCase(unittest.TestCase):
    hugo_langs = ['ca-va', 'uk']

    def test_dir_fetch(self):
        os.environ['PACKAGE'] = 'websites-apps-kde-org'
        po_path = 'pos_dir'
        fetch_langs(self.hugo_langs, po_path, svn_branch='l10n-kf5')
        lang_dirs = next(os.walk(po_path))[1]
        self.assertEqual({'ca@valencia', 'uk'}, set(lang_dirs))
        ca_files = next(os.walk(f'{po_path}/ca@valencia'))[2]
        self.assertEqual(len(ca_files), 2)
        shutil.rmtree(po_path)

    def test_file_fetch(self):
        os.environ['PACKAGE'] = 'plasma-workspace'
        os.environ['FILENAME'] = 'plasma-workspace._desktop_'
        po_path = 'pos_file'
        fetch_langs(self.hugo_langs, po_path, as_static=False)
        lang_files = next(os.walk(po_path))[2]
        self.assertEqual({'ca@valencia.po', 'uk.po'}, set(lang_files))
        shutil.rmtree(po_path)


if __name__ == '__main__':
    unittest.main()
