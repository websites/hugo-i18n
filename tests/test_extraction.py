# SPDX-FileCopyrightText: 2021 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-or-later

import importlib.resources as pkg_resources
import os
import unittest
from argparse import Namespace

from hugo_gettext.extraction.e_domain import HugoDomainE
from hugo_gettext.extraction.renderer_hugo_i18n import RendererHugoI18N
from markdown_it import MarkdownIt
from mdit_py_hugo.shortcode import shortcode_plugin
from mdit_py_plugins.front_matter import front_matter_plugin


class ExtractionTestCase(unittest.TestCase):
    package = 'websites-kde-org'
    os.environ['PACKAGE'] = package
    mdi = MarkdownIt(renderer_cls=RendererHugoI18N).use(front_matter_plugin).use(shortcode_plugin) \
        .enable('table')
    e = Namespace(hg_config=Namespace(shortcodes={}))

    def test_extract_content(self):
        domain_e = HugoDomainE(self.e)
        env = {
            'path': pkg_resources.path('tests.resources', 'distros.md'),
            'domain_extraction': domain_e,
            'with_line': True
        }
        with pkg_resources.open_text('tests.resources', 'distros.md') as f_dist:
            doc_tokens = self.mdi.parse(f_dist.read())
            doc_tokens.pop(0)
        self.mdi.renderer.render(doc_tokens, self.mdi.options, env)
        self.assertEqual(7, len(domain_e.entries))

    def test_extract_blockquotes(self):
        domain_e = HugoDomainE(None)
        env = {
            'path': pkg_resources.path('tests.resources', 'blockquotes.md'),
            'domain_extraction': domain_e,
            'with_line': True
        }
        with pkg_resources.open_text('tests.resources', 'blockquotes.md') as f_bq:
            doc_tokens = self.mdi.parse(f_bq.read())
            doc_tokens.pop(0)
        self.mdi.renderer.render(doc_tokens, self.mdi.options, env)
        self.assertEqual(12, len(domain_e.entries))

    def test_extract_codeblock(self):
        domain_e = HugoDomainE(None)
        env = {
            'path': pkg_resources.path('tests.resources', 'codeblock.md'),
            'parse_fence': True,
            'domain_extraction': domain_e,
            'with_line': True
        }
        with pkg_resources.open_text('tests.resources', 'codeblock.md') as f_cb:
            doc_tokens = self.mdi.parse(f_cb.read())
            doc_tokens.pop(0)
        self.mdi.renderer.render(doc_tokens, self.mdi.options, env)
        self.assertEqual(6, len(domain_e.entries))


if __name__ == '__main__':
    unittest.main()
