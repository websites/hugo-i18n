---
authors:
- SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu@kde.org>
SPDX-License-Identifier: CC0-1.0
---
term number 1
: details number 1

term number 2
: details number 2
onto the next line, without indentation

term number 3
: details number 3
  onto the next line, with indentation

term with list
: details with list
  - a list item
  - another list item
  
  another list
  1. item number 1
  2. item number 2

term with blockquote
: details with blockquote
  > a block quote
  > with two lines

term with headings
: details with headings
  ## heading level 2
  text under heading level 2
  
  a setext heading
  -
  text under setext heading level 2

term with fenced code block
: details with fenced code block
  ```
  def main:
      pass
  ```

term with indented code block
: details with indented code block
  
  before the code block
  
      def main:
          pass
  after the code block

term with table
: details with table
  Col One | Col Two
  ------- | -------
  Row1 col1 | Row1 col2
  Row2 col1 | Row2 col2

term with horizontal rule
: details with horizontal rule
  
  before horizontal rule
  
  ---
  after horizontal rule