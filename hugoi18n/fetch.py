# SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-or-later

import logging
import os
import subprocess

from . import customs

_lang_code_at_dict = {
    'cy': 'cyrillic',
    'ije': 'ijekavian',
    'il': 'ijekavianlatin',
    'la': 'latin',
    'va': 'valencia'
}


def revert_lang_code(hugo_lang_code):
    if hugo_lang_code == 'pt-pt':
        return 'pt'
    else:
        parts = hugo_lang_code.split('-')
        if len(parts) > 1:
            if parts[1] in _lang_code_at_dict:
                return f'{parts[0]}@{_lang_code_at_dict[parts[1]]}'
            else:
                return f'{parts[0]}_{parts[1].upper()}'
        else:
            return hugo_lang_code


def fetch_langs(hugo_langs, po_path: str, file_ext='po', svn_branch='l10n-kf6', as_static: bool = True):
    """
    Fetch translations from SVN
    :param hugo_langs: Hugo language codes of languages to fetch
    :param po_path: directory to store translations
    :param file_ext: extension of the files to fetch, default to 'po'
    :param svn_branch: branch to fetch from, default to 'l10n-kf6'
    :param as_static: whether the file was made on the server as a static file
    :return:
    """
    package = os.environ['PACKAGE']
    svn_pre_path = f'svn://anonsvn.kde.org/home/kde/trunk/{svn_branch}'
    svn_sub_path = f'messages/{package}'
    as_file = bool(os.environ.get('FILENAME', ''))

    os.makedirs(po_path, exist_ok=True)
    for hugo_lang in hugo_langs:
        lang = revert_lang_code(hugo_lang)
        svn_path = f'{svn_pre_path}/{lang}/{svn_sub_path}'

        if not as_file:
            output = f'{po_path}/{lang}'
        else:
            file_name = customs.get_default_domain_name(package)
            if as_static:
                file_name += '._static_'
            svn_path += f'/{file_name}.{file_ext}'
            output = f'{po_path}/{lang}.{file_ext}'

        try:
            subprocess.check_output(['svn', 'export', svn_path, f'{output}@'], stderr=subprocess.PIPE)
            logging.info(f"Fetched {lang}")
        except subprocess.CalledProcessError:
            logging.info(f"Non-existent {lang}")
