# SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-or-later

import importlib.resources as pkg_resources
import os
from typing import Dict, Set, List

import yaml

_default_domain_names = {
    'websites-timeline-kde-org': 'timeline-kde-org-shared'
}
_trans_table = str.maketrans('@_', '--')
_custom_excluded_keys = {'appstream', 'authors', 'background', 'cdnJsFiles', 'cssFiles', 'flatpak_exp',
                         'forum', 'jsFiles', 'hl_class', 'hl_video', 'konqi', 'minJsFiles', 'parent',
                         'sassFiles', 'screenshot', 'scssFiles', 'SPDX-License-Identifier', 'src_icon',
                         'userbase'}
_rtl_langs = ['ar', 'he']


def load_lang_names() -> Dict[str, str]:
    with pkg_resources.open_text('hugoi18n.resources', 'languages.yaml') as f_langs:
        lang_names = yaml.safe_load(f_langs)
    return lang_names


def get_default_domain_name(package: str) -> str:
    if po_file := os.environ.get('FILENAME', ''):
        return po_file
    return _default_domain_names.get(package, '-'.join(package.split('-')[1:]))


def convert_lang_code(lang_code: str) -> str:
    if lang_code == 'pt':
        return 'pt-pt'
    else:
        hugo_lang_code = lang_code.lower().translate(_trans_table)
        parts = hugo_lang_code.split('-')
        if len(parts) > 1:
            if parts[1] == 'ijekavian':
                hugo_lang_code = f'{parts[0]}-ije'
            elif parts[1] == 'ijekavianlatin':
                hugo_lang_code = f'{parts[0]}-il'
            elif len(parts[1]) > 2:
                hugo_lang_code = f'{parts[0]}-{parts[1][0:2]}'
        return hugo_lang_code


def get_custom_excluded_keys() -> Set[str]:
    return _custom_excluded_keys


def get_pot_fields() -> Dict[str, str]:
    return {
        'report_address': 'https://bugs.kde.org',
        'team_address': 'kde-i18n-doc@kde.org'
    }


def get_rtl_langs() -> List[str]:
    return _rtl_langs


def get_parse_fence() -> bool:
    return True
