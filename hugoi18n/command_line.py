# SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
# SPDX-FileCopyrightText: 2021-2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-or-later

import importlib.resources as pkg_resources
import logging
import os
import subprocess
from argparse import ArgumentParser, RawTextHelpFormatter

from yaml import safe_load
import hugo_gettext.extraction as extraction
import hugo_gettext.generation as generation
import hugo_gettext.compilation as compilation

from . import customs
from .fetch import fetch_langs


def main():
    parser = ArgumentParser(description='I18n tool for Hugo projects, working in coordination with scripty')
    parser.add_argument('-q', '--quiet', action='store_true', help='stop showing INFO or lower logs')
    subparsers = parser.add_subparsers(description="used in the process from extracting source files' messages "
                                                   'to generating target files')

    extract_cmd = subparsers.add_parser('extract', help='extract messages from source files',
                                        formatter_class=RawTextHelpFormatter)
    extract_cmd.add_argument('pot', help='either path of the only target pot file or path of the directory\n'
                                         'containing the target pot file(s)')
    extract_cmd.set_defaults(func=extract)

    compile_po_cmd = subparsers.add_parser('compile', help='compile translated messages to binary format',
                                           formatter_class=RawTextHelpFormatter)
    compile_po_cmd.add_argument('dir', help='path of the directory containing either PO files named as {lang}.po, or\n'
                                            'directories with PO files inside {lang}/*.po')
    compile_po_cmd.set_defaults(func=compile_po)

    generate_cmd = subparsers.add_parser('generate', help='generate target messages and files',
                                         formatter_class=RawTextHelpFormatter)
    generate_cmd.add_argument('-k', '--keep-locale', action='store_true', help='do not delete locale folder')
    generate_cmd.set_defaults(func=generate)

    fetch_cmd = subparsers.add_parser('fetch', help='fetch PO files from KDE SVN',
                                      formatter_class=RawTextHelpFormatter)
    fetch_cmd.add_argument('dest', help='destination directory')
    fetch_cmd.add_argument('-l', '--langs', nargs='*', help='list of codes of languages to fetch')
    fetch_cmd.add_argument('-b', '--branch', help='SVN branch to fetch from')
    fetch_cmd.set_defaults(func=fetch)

    args = parser.parse_args()
    level = logging.WARNING if args.quiet else logging.INFO
    logging.basicConfig(format='%(levelname)s: %(message)s', level=level)
    args.func(args)


def extract(args):
    """
    Extract messages from source files.
    The input to hugo-gettext is a directory path. It then puts all POT files in there. PO_DIR is fine with this.
    For PO_FILE, the default domain needs to be set as the `FILENAME` env. var.
    We then let hugo-gettext do its job in the dir part of `args.pot`, and rename the default pot to the file part.
    :param args: arguments passed in command line, containing
        - pot: either path of the only target pot file or path of the directory containing the target pot file(s)
    :return: None
    """
    args.__setattr__('customs', customs.__file__)
    args.__setattr__('config', None)
    if po_file := os.environ.get('FILENAME', ''):
        # po_file is the default domain name
        target = os.path.abspath(args.pot)
        dirname = os.path.dirname(target)
        args.__setattr__('pot', dirname)
        extraction.extract(args)
        src_path = os.path.join(dirname, f'{po_file}.pot')
        os.replace(src_path, target)
    else:
        extraction.extract(args)


def generate(args):
    """
    Generate target messages and files
    :return: None
    """
    args.__setattr__('customs', customs.__file__)
    args.__setattr__('config', None)
    generation.generate(args)


def compile_po(args):
    """
    Compile translated messages to binary format stored in 'locale/{lang}/LC_MESSAGES' directory
    :param args: arguments passed in command line, containing
        - dir: path of the directory containing either:
            - PO files named as {lang}.po, or
            - directories with PO files inside {lang}/*.po
    :return: None
    """
    po_dir = args.dir
    if domain_name := os.environ.get('FILENAME', ''):
        for file in os.listdir(po_dir):
            if os.path.splitext(file)[1] == '.pot':
                continue
            lang = os.path.splitext(file)[0]

            target_path = f'locale/{lang}/LC_MESSAGES'
            os.makedirs(target_path, exist_ok=True)
            src = f'{po_dir}/{file}'

            command = f'msgfmt {src} -o {target_path}/{domain_name}.mo'
            subprocess.run(command, shell=True, check=True)
            logging.info(f'Compiled {lang}')
    else:
        compilation.compile_po(args)


def fetch(args):
    hugo_langs = args.langs
    if not hugo_langs:
        with pkg_resources.open_text('hugoi18n.resources', 'languages.yaml') as f_langs:
            lang_names = safe_load(f_langs)
            hugo_langs = lang_names.keys()
    if args.branch:
        fetch_langs(hugo_langs, args.dest, svn_branch=args.branch)
    else:
        fetch_langs(hugo_langs, args.dest)
