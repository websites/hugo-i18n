#!/usr/bin/python

# SPDX-FileCopyrightText: 2021-2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-or-later

"""
Get names of languages that KDE supports
In an environment with hugo-i18n installed, run: python lang_names.py
"""

import configparser
import logging
import os
import subprocess
import tempfile

import icu
import yaml

from hugoi18n.customs import convert_lang_code
from hugoi18n.fetch import fetch_langs

manual_names = {
    'crh': 'Къырым Татар',  # https://en.wikipedia.org/wiki/ISO_639:c
    'ha': 'حَوْسَ',  # https://en.wikipedia.org/wiki/ISO_639:h
    'hne': 'छत्तीसगढ़ी',  # https://en.wikipedia.org/wiki/Chhattisgarhi_language
    'zh_HK': '中文（香港）'  # taken from PyICU, but no need to keep all the "SAR of China" shit
}


def get_lang_name(base_lang_dir, l_code):
    file_path = f'{base_lang_dir}/{l_code}.desktop'
    with open(file_path) as ini:
        config = configparser.ConfigParser()
        config.read_file(ini)
        section = config['KCM Locale']
        l_key = f'Name[{l_code}]'
        if l_key in section:
            l_name = section[l_key]
        elif l_code in manual_names:
            l_name = manual_names[l_code]
        else:
            locale = icu.Locale(l_code)
            try:
                l_name = locale.getDisplayName(locale)
            except icu.InvalidArgsError:
                print(f'error with language code {l_code}')
    return l_name


def get(base_lang_dir):
    teams_url = 'svn://anonsvn.kde.org/home/kde/trunk/l10n-kf5/subdirs'
    teams_path = f'{base_lang_dir}/subdirs'
    try:
        subprocess.check_output(['svn', 'export', teams_url, f'{teams_path}@'], stderr=subprocess.PIPE)
        logging.info('Fetched team codes')
    except subprocess.CalledProcessError:
        logging.info('Non-existent file of team codes')

    with open(teams_path) as f_teams:
        # there's a newline at the end of each line
        team_codes = [line[:-1] for line in f_teams.readlines()]
        hugo_langs = [convert_lang_code(c) for c in team_codes]
    os.remove(teams_path)

    os.environ['PACKAGE'] = 'kconfigwidgets'
    os.environ['FILENAME'] = 'kf6_entry'
    fetch_langs(hugo_langs, po_path=base_lang_dir, file_ext='desktop', as_static=False)
    languages = {'en': 'English'}
    for f in os.listdir(base_lang_dir):
        lang_code = os.path.splitext(f)[0]
        lang_name = get_lang_name(base_lang_dir, lang_code)
        hugo_lang_code = convert_lang_code(lang_code)
        languages[hugo_lang_code] = lang_name
    with open('hugoi18n/resources/languages.yaml', 'w') as lang_yaml:
        lang_yaml.write('# SPDX''-FileCopyrightText: 2023 Phu Hung Nguyen <phu.nguyen@kdemail.net>\n'
                        '# SPDX''-License-Identifier: CC0-1.0\n\n')
        yaml.dump(languages, lang_yaml, default_flow_style=False, allow_unicode=True)


if __name__ == "__main__":
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    with tempfile.TemporaryDirectory() as tmp_dir:
        get(tmp_dir)
